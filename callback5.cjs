/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/


const getBoardData = require('./callback1.cjs');
const getCardsAccordingToList = require('./callback3.cjs');
const getListAccordingTOBoardId = require('./callback2.cjs');

function getDataFromCards(boards,list,cards){
    try {
        setTimeout(() => {
            getBoardData("mcu453ed", boards , (board)=>{
                console.log(board);
                getListAccordingTOBoardId("mcu453ed",list,(listData)=>{
                    console.log(listData);
                    const mindSpaceId = listData.filter((key)=>key.name === "Mind" || key.name === "Space" ).map((key)=> key.id);
                    //console.log(mindId);
                    
                    mindSpaceId.forEach((key)=>{
                        getCardsAccordingToList(key,cards,(card)=>{
                            console.log(card);
                        })
                    })
                    
                });

            })
        }, 2000);
      } catch (error) {
        console.log(error.message);
      }
}

module.exports = getDataFromCards;