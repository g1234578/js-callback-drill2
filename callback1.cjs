/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

function getBoardData(boardID, boards, callback) {
  try {
    setTimeout(() => {
      const board = boards.find((key) => key.id === boardID);

      callback(board);
    }, 2000);
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = getBoardData;
