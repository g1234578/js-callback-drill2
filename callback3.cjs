/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

function getCardsAccordingToList(listId, cards, callback) {
  try{
    setTimeout(() => {
        const cardsList = cards[listId];
        callback(cardsList);
      }, 4000);
  }
  catch(error){
    console.log(error.message);
  }
}

module.exports = getCardsAccordingToList;