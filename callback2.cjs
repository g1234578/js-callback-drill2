/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

function getListAccordingTOBoardId(boardID, list, callback) {
  try{
    setTimeout(() => {
        let listOfBoardId = list[boardID];
        callback(listOfBoardId);
      },3000);
  }
  catch(error){
    console.log(error.message);
  }
}

module.exports = getListAccordingTOBoardId;
